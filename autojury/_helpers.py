import sys

def get_input(msg, type, check=None, default=None):
    while True:
        sys.stdout.flush()

        value = input(msg + (f" [{default}]" if default is not None else '') + ': ')
        if not value and default is not None:
            value=default
        try:
            typed_value = type(value)
        except (ValueError,TypeError):
            print("Invalid type. Please try again.")
            continue
        if check is None or check(typed_value):
            return typed_value
        print("Incorrect value (check failed). Please try again.")
