import argparse

from . import _procedure as procedure

def get_parser():
    parser = argparse.ArgumentParser('autojury')
    parser.add_argument('--input', '-i', help='input csv', required=True,
                        type=argparse.FileType('rb'))
    parser.add_argument('--output', '-o', help='output csv', required=True,
                        type=argparse.FileType('wt'))
    return parser

def main():
    parser = get_parser()
    args = parser.parse_args()
    procedure.process(args.input, args.output)
