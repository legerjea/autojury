
from typing import List, Tuple

import numpy as np
import scipy as sp

EVO_FACTOR = 1.001

class MCMC_posterior:
    __slots__ =    ('_expectation_prior_slope','_data','_response','_current_threshold','_current_slope','_current_loglik','_spropo_threshold','_spropo_slope')
    _expectation_prior_slope: float
    _data: List[Tuple[float]]
    _current_threshold: float
    _current_slope: float
    _current_loglik: float|None
    _spropo_threshold: float
    _spropo_slope: float

    def __init__(self,data, expectation_prior_slope=5.):
        self._data = np.array(data)
        self._response = np.zeros(data.shape)
        self._current_threshold = 10.
        self._current_slope = expectation_prior_slope
        self._current_loglik = None
        self._spropo_threshold = 10.
        self._spropo_slope = 1.
        self._expectation_prior_slope = expectation_prior_slope

    def _compute_loglik(self, threshold, slope):
        if threshold<0 or threshold>20:
            return -np.inf
        if slope<0:
            return -np.inf

        return -slope/self._expectation_prior_slope+sp.special.log_expit(slope*(self._data-threshold)*self._response).sum()


    def _run_chain_one(self):
        if self._current_loglik is None:
            self._current_loglik = self._compute_loglik(self._current_threshold,
                                                        self._current_slope)
        
        propo_treshold = self._current_threshold + self._spropo_threshold*np.random.normal()
        new_loglik = self._compute_loglik(propo_treshold, self._current_slope)
        if np.log(np.random.uniform())<new_loglik-self._current_loglik:
            self._current_threshold = propo_treshold
            self._current_loglik = new_loglik
            self._spropo_threshold*=1.001
        else:
            self._spropo_threshold*=(EVO_FACTOR**.3)**(-1/(1-.3))
        
        propo_slope = self._current_slope + self._spropo_slope*np.random.normal()
        new_loglik = self._compute_loglik(self._current_threshold, propo_slope)
        if np.log(np.random.uniform())<new_loglik-self._current_loglik:
            self._current_slope = propo_slope
            self._current_loglik = new_loglik
            self._spropo_slope*=1.001
        else:
            self._spropo_slope*=(EVO_FACTOR**.3)**(-1/(1-.3))

    def set_response(self, i, value):
        self._response[i] = (1 if value else -1)
        self._current_loglik = None

    def run_and_get_a_posteriors(self, samples = 10**5):
        obsval = np.zeros((samples, self._data.size))
        for i in range(samples):
            self._run_chain_one()
            obsval[i] = self._current_slope*(self._data-self._current_threshold)
        return sp.special.expit(obsval).mean(axis=0)

    def run_many(self, samples=10**5):
        for _ in range(samples):
            self._run_chain_one()

    def run_and_get_threshold(self, samples=10**6):
        thresholds = np.zeros(samples)
        for i in range(samples):
            self._run_chain_one()
            thresholds[i] = self._current_threshold
        return thresholds.mean()

    @property
    def free_students(self):
        return self._response==0



        
        



