import io
import csv
import dataclasses
import itertools
import bisect

import numpy as np
import cchardet
import colorama

from . import _helpers
from . import _mcmc as mcmc


def isfloat(x):
    if not x:
        return True
    try:
        float(x)
    except ValueError:
        return False
    return True


SEP = "\n" + "=" * 80 + "\n"


@dataclasses.dataclass
class DataDesc:
    data: tuple[dict]
    displayed_columns: tuple
    grades: tuple
    grade_column: str
    all_columns: tuple
    decisions: list[bool | None]
    results: tuple|None = None

    def to_csv(self, output_fd: io.BytesIO):
        ordered_indexes = sorted(
            range(len(self.data)), key=lambda i: self.grades[i]
        )
        available_columns = list(dict((x,None) for x in self.all_columns)|{'Note':None})

        formater = "{i:" + f"{int(np.ceil(np.log10(len(available_columns))))}" + "d}. {c!r}"
        for i, c in enumerate(available_columns):
            print(formater.format(i=i, c=c))
        idx_note = next(i for i,c in enumerate(available_columns) if c == 'Note')
        print()
        selected_indexes = _helpers.get_input(
            f"Select output columns (with display order) during"
            " the decision process\n(indexes separated by commas)",
            type=lambda x: [int(y) for y in x.split(",")],
            check=lambda x: len(x) == len(set(x))
            and all(0 <= y < len(available_columns) for y in x)
            and idx_note in x,
        )
        columns = [available_columns[i] for i in selected_indexes]
        writer = csv.DictWriter(output_fd, dialect=csv.excel, fieldnames =
                                columns)
        writer.writeheader()
        for i in ordered_indexes:
            writer.writerow({c: self.results[i] if c=='Note' else self.data[i][c] for c in
             columns})

    @classmethod
    def from_csv(cls, input_fd: io.BytesIO):
        input_content = input_fd.read()
        enc = cchardet.detect(input_content)
        encoding = enc["encoding"]
        conf = enc["confidence"]
        if encoding is None or conf<.9:
            encoding = 'utf8'
        decoded_content = input_content.decode(encoding)
        dialect = csv.Sniffer().sniff(decoded_content)
        reader = csv.DictReader(io.StringIO(decoded_content), dialect=dialect)
        rows = tuple(reader)
        columns = tuple(reader.fieldnames)
        float_columns = [c for c in columns if all(isfloat(r[c]) for r in rows)]
        print(SEP)
        if not float_columns:
            raise ValueError("No elligible (only floats) column found for global grade")
        if len(float_columns) == 1:
            grade_column = float_columns[0]
            print(f"Grade column: automatically selected {grade_column!r}")
        else:
            print(f"Grade column: found {len(float_columns)} elligible columns:")
            print()
            for i, c in enumerate(float_columns):
                print(f"  {i}. {c!r}")
            print()
            grade_column = float_columns[
                _helpers.get_input(
                    f"Select a index",
                    type=int,
                    check=lambda x: 0 <= x < len(float_columns),
                )
            ]
            print(f"Grade column: selected {grade_column!r}")
        grades = tuple(float(x) if x else 0.0 for x in (r[grade_column] for r in rows))
        print(SEP)
        formater = "{i:" + f"{int(np.ceil(np.log10(len(columns))))}" + "d}. {c!r}"
        print("Columns are:")
        print()
        for i, c in enumerate(columns):
            print(formater.format(i=i, c=c))
        print()
        displayed_indexes = _helpers.get_input(
            f"Select displayed columns (with display order) during"
            " the decision process\n(indexes separated by commas)",
            type=lambda x: [int(y) for y in x.split(",")],
            check=lambda x: len(x) == len(set(x))
            and all(0 <= y < len(columns) for y in x),
        )
        displayed_columns = tuple(columns[c] for c in displayed_indexes)
        print(
            "Displayed columns are: " + ", ".join(f"{c!r}" for c in displayed_columns)
        )
        return cls(
            data=rows,
            displayed_columns=displayed_columns,
            grades=grades,
            grade_column=grade_column,
            all_columns=columns,
            decisions=[
                None,
            ]
            * len(rows),
        )


def phase1_threshold(datadesc: DataDesc):
    mc = mcmc.MCMC_posterior(np.array(datadesc.grades))
    for it_count in itertools.count(start=1):
        mc.run_many()
        posteriors = mc.run_and_get_a_posteriors()
        undet = (np.abs(posteriors - 0.5) < 0.25) & mc.free_students
        if undet.sum() == 0:
            break
        print(SEP)
        print(f"Step: {it_count}")
        print(f"# unknown decision w.r.t. posterior probability: {undet.sum()}")
        print()
        selected = np.random.choice(np.where(undet)[0])
        print("Please examine the following student:")
        print()
        for c in datadesc.displayed_columns:
            print(f"  - {c}: {datadesc.data[selected][c]}")
        print()
        decision = (
            _helpers.get_input(
                "Enter your decision (Pass or Fail)",
                type=str,
                check=lambda x: x.lower() in ("pass", "fail"),
            ).lower()
            == "pass"
        )
        mc.set_response(selected, decision)
        datadesc.decisions[selected] = decision
    print(SEP)
    print("Found threshold: ", end="")
    threshold = mc.run_and_get_threshold()
    print(f"{threshold:.3f}")
    return threshold


STYLE1 = colorama.Back.LIGHTWHITE_EX + colorama.Fore.BLACK
STYLE2 = colorama.Back.WHITE + colorama.Fore.BLACK
SPASS = (
    colorama.Back.GREEN + colorama.Fore.LIGHTWHITE_EX + colorama.Style.BRIGHT + " PASS "
)
SFAIL = (
    colorama.Back.RED + colorama.Fore.LIGHTWHITE_EX + colorama.Style.BRIGHT + " FAIL "
)


def passfail(x, STYLE):
    return (SPASS if x else SFAIL) + STYLE


SRESET = colorama.Style.RESET_ALL


def phase2_fine_threshold(datadesc: DataDesc, threshold: float, cols=100):
    litigious = [
        i
        for i, (grade, dec) in enumerate(zip(datadesc.grades, datadesc.decisions))
        if dec is not None and (dec ^ (grade >= threshold))
    ]
    if not litigious:
        print("Skipped: all decisions are consistants")
        return threshold

    print("Some of decisions are not consistant with inferred threshold.")
    print(
        "Here, the war zone in displayed (all cases of inconsistancy and examinated adjacent cases)."
    )
    print()
    litigious_grades = [datadesc.grades[i] for i in litigious] + [threshold]
    min_litigious_grades = min(litigious_grades)
    max_litigious_grades = max(litigious_grades)
    war_zone = set(
        i
        for i, grade in enumerate(datadesc.grades)
        if min_litigious_grades <= grade <= max_litigious_grades
    )
    ordered_indexes = sorted(
        range(len(datadesc.data)), key=lambda i: datadesc.grades[i]
    )
    war_zone.update(
        itertools.takewhile(
            lambda i: datadesc.decisions[i] is not None,
            itertools.dropwhile(
                lambda i: datadesc.grades[i] <= max_litigious_grades, ordered_indexes
            ),
        )
    )
    war_zone.update(
        itertools.takewhile(
            lambda i: datadesc.decisions[i] is not None,
            itertools.dropwhile(
                lambda i: datadesc.grades[i] >= min_litigious_grades,
                reversed(ordered_indexes),
            ),
        )
    )

    war_zone = sorted(war_zone, key=lambda i: datadesc.grades[i])

    used_columns = [c for c in datadesc.displayed_columns if c != datadesc.grade_column]
    used_columns.append(datadesc.grade_column)
    colsize = (cols - 1 - 2 * (3 + 4)) // len(used_columns) - 3
    colshort = lambda y: " " + (
        lambda x: (x + " " * (1 + colsize - len(x)))
        if len(x) <= colsize
        else (x[:colsize] + "…")
    )(("{x:" + f"{colsize}" + ".2f}").format(x=float(y)) if isfloat(y) else f"{y}")
    sepline = (
        "{begin}"
        + ("\u2500" * (colsize + 2) + "{mid}") * len(used_columns)
        + "\u2500" * 6
        + "{mid}"
        + "\u2500" * 6
        + "{end}"
        + SRESET
    )
    line = (
        STYLE1
        + "\u2502{STYLE}"
        + "\u2502".join(("%s",) * (len(used_columns) + 2))
        + STYLE1
        + "\u2502"
        + SRESET
    )
    print(STYLE1 + sepline.format(begin="\u250c", mid="\u252c", end="\u2510"))
    print(
        line.format(STYLE=STYLE1)
        % (tuple(colshort(c) for c in used_columns) + (" Auto ", " Manu "))
    )
    print(STYLE1 + sepline.format(begin="\u251c", mid="\u253c", end="\u2524"))
    for k, i in enumerate(reversed(war_zone)):
        style = STYLE2 if k % 2 == 0 else STYLE1
        print(
            line.format(STYLE=style)
            % (
                tuple(colshort(datadesc.data[i][c]) for c in used_columns)
                + (
                    passfail(datadesc.grades[i] >= threshold, style),
                    passfail(datadesc.decisions[i], style)
                    if datadesc.decisions[i] is not None
                    else "      ",
                )
            )
        )
    print(STYLE1 + sepline.format(begin="\u2514", mid="\u2534", end="\u2518"))

    return _helpers.get_input(
        "Please adapt your threshold if needed and give definitive threshold",
        type=float,
        check=lambda x: 0 < x < 20,
    )


def phase3_letters(datadesc: DataDesc, threshold):
    fx_propo = _helpers.get_input(
        f"Considering only {SFAIL}{SRESET} grades, "
        "please enter the proportion of FX,\n"
        "(enter a number in [0,1], 1 for FX only, 0 for F only)",
        type=float,
        check=lambda x: 0 <= x <= 1,
    )
    print()
    letters_propo = _helpers.get_input(
        f"Considering only {SPASS}{SRESET} grades, "
        "please enter the proportion for A, B, C, D, E,\n"
        "separated by commas. (default: ECTS advised values)",
        type=lambda x: np.array([float(y) for y in x.split(",")]),
        check=lambda x: x.size == 5 and (x >= 0).all() and np.isclose(x.sum(), 1),
        default="0.10, 0.25, 0.30, 0.25, 0.10",
    )
    
    passing = sorted((i for i,grade in enumerate(datadesc.grades) if
                      grade>=threshold), key=lambda i: -datadesc.grades[i])
    failing = sorted((i for i,grade in enumerate(datadesc.grades) if
                      grade<threshold), key=lambda i: -datadesc.grades[i])
    letters_propo = np.array(letters_propo)
    cumsum_letters_propo = np.cumsum(letters_propo)/letters_propo.sum()
    result = dict(zip(passing,('ABCDE'[bisect.bisect(cumsum_letters_propo,x)] for x in
    np.arange(1,2*len(passing),2)/(2*len(passing)))))
    result.update({i: 'FX' if r<fx_propo else 'F' for i,r in
     zip(failing,np.arange(1,2*len(failing),2)/(2*len(failing)))})
    datadesc.results = tuple(result[i] for i in range(len(datadesc.data)))

def process(fd_input, fd_output):
    data = DataDesc.from_csv(fd_input)
    threshold = phase1_threshold(data)
    threshold = phase2_fine_threshold(data, threshold)
    phase3_letters(data, threshold)
    data.to_csv(fd_output)

